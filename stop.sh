#!/bin/bash


# NEW
challenges_dir=./challenges
compose_file=docker/docker-compose.yml
logfile=./dockers-starts.log

for categorie in `ls $challenges_dir`;
do
    echo "[*] Category : $categorie";
    for challenge in `ls $challenges_dir/$categorie`;
    do
        echo "--> [*] Challenge $challenge";
        chall_dir=$challenges_dir/$categorie/$challenge
        stop_file=$chall_dir/stop.sh
            if [ -f "$stop_file" ]; then
            echo "--> [*] Stopping challenge $challenge";
	    # pushd $chall_dir; docker-compose down; popd
	    pushd $chall_dir; ./stop.sh; popd
            echo "$challenge:STOPPED" >> $logfile
        else
            echo "--> [X] No stop file for chall $challenge.";
            echo "$challenge:NULL" >> $logfile
        fi;
    done;
done;

echo "Killing autoheal"
screen -S heal -X quit

