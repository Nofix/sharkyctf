#!/usr/bin/python3


map_file="mapping.txt"

def get_map():
    ports_map = []
    with open(map_file, 'r') as f:
        a = f.readlines()
    for l in a:
        splited = l.split(':')
        ports_map.append((splited[0], splited[1][:-1]))
    return ports_map

def get_nginx_cfg_https(ports_map):
    cfg = "ssl_certificate /etc/letsencrypt/live/sharkyctf.xyz/fullchain.pem;\n"
    cfg += "ssl_certificate_key /etc/letsencrypt/live/sharkyctf.xyz/privkey.pem;\n"
    for i in ports_map:
        cfg += (
                "upstream " + str(i[0]) + " {" + "\n\t"
                + "server 127.0.0.1:" + str(i[1]) + ";\n}\n"
            )
        cfg += (
               """server {
    listen 443 ssl;
    server_name """ + str(i[0]) + """.sharkyctf.xyz;
    location / {

        proxy_set_header        Host $host;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;

        proxy_pass          http://""" + str(i[0]) + """;
        proxy_read_timeout  20;
    }
}

"""
            )
    return cfg

def get_nginx_cfg_http(ports_map):
    cfg = ""
    for i in ports_map:
        cfg += (
                "upstream " + str(i[0]) + " {" + "\n\t"
                + "server 127.0.0.1:" + str(i[1]) + ";\n}\n"
            )
        cfg += (
               """server {
    listen 80;
    server_name """ + str(i[0]) + """.sharkyctf.xyz;
    location / {

        proxy_set_header        Host $host;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;

        proxy_pass          http://""" + str(i[0]) + """;
        proxy_read_timeout  20;
    }
}

"""
            )
    return cfg

ports_map = get_map()
print(get_nginx_cfg_http(ports_map))
