#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#define FALSE        0
#define TRUE         1
#define SEEK_DATA    3
#define BLOCK_NUMBER 5000

#ifndef FLAG_FILE
  #define FLAG_FILE  "my_very_big_file"
#endif
#ifndef DEBUG
  #define DEBUG      FALSE
#endif

int main(int argc, char const *argv[]) {
  int fd = open(FLAG_FILE, O_RDONLY);

  if (DEBUG) {
    struct stat st;
    stat("./"FLAG_FILE, &st);
    printf("%ld\n", st.st_size);
  }

  char flag_char = 0;
  __uint128_t off = 0;

  while ((off = lseek(fd, off, SEEK_DATA)) != -1 && errno != ENXIO) {
    off++;

    if (DEBUG) printf("Off = %lld\n", off);

    read(fd, &flag_char, sizeof(char));

    if (flag_char != 0) {
      printf("%c", flag_char);
    }
  }
  fflush(stdout);
  close(fd);


  return 0;
}
