const mongoose = require('mongoose');
const bodyParser = require('body-parser');
var express = require('express'); // Get the module
var app = express();

app.use(express.static('public'))
// connect to Mongo daemon
mongoose
  .connect(
    'mongodb://containment_forum-mongo:27017/express-mongo',
    { useNewUrlParser: true }
  )
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));


// DB schema
const ItemSchema = new mongoose.Schema({
  is_active: {
    type: Boolean,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  pseudo: {
    type: String,
    required: true
  },
  details: {
    type: String,
    required: true
  },
  image: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});

Item = mongoose.model('item', ItemSchema);
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  Item.find((err, items) => {
    if (err) throw err;
    res.render("index", { items });
  });
});

app.get("/confinement", (req, res) => {
  Item.find({"category":"containment"},(err, items) => {
    if (err) throw err;
    res.render("items", { items });
  });
});

app.get("/flag", (req, res) => {
  Item.find({"category":"flag"},(err, items) => {
    if (err) throw err;
    res.render("items", { items });
  });
});

app.get("/item/:number([0-9a-f]{24})", (req, res) => {

  Item.findOne({ '_id': req.params.number }, (err, item) => {
    if (err) throw err;
    res.render("item", { item });
  });
});

const port = 3000;
app.listen(port, () => console.log('Server running...'));
