db.items.insert({"_id" : ObjectId("5e70da94d7b1600013655bb5"),"is_active" : true,"category" : "containment","pseudo" : "Thotonox","name" : "Confinement basics","details" : "When I will finally meet another human being after 3 months of confinement.","image" : "/img/surprised.gif","date" : ISODate("2020-03-17T14:11:32.471Z"),"__v" : 0})
db.items.insert({
    "_id" : ObjectId("5e75dab2d7b1600013655bb8"),
    "is_active" : false,
    "category" : "flag",
    "pseudo" : "N1CE_ONE",
    "name" : "Flag_first_part",
    "details" : "shkCTF{IDOR_IS_ALS0_",
    "image" : "/img/flag_part_one.gif",
    "date" : ISODate("2020-03-21T09:13:22.254Z"),
    "__v" : 0
})

db.items.insert({
    "_id" : ObjectId("5e7e4f48d7b1600013655bb9"),
    "is_active" : true,
    "category" : "containment",
    "pseudo" : "enstro",
    "name" : "Confined together",
    "details" : "Being with another person can also be tough",
    "image" : "/img/containment_with_someone.gif",
    "date" : ISODate("2020-03-27T19:08:56.736Z"),
    "__v" : 0
})

db.items.insert({
    "_id" : ObjectId("5e83642bd7b1600013655bba"),
    "is_active" : true,
    "category" : "containment",
    "pseudo" : "DarkClown",
    "name" : "Eating anything",
    "details" : "Seeing people buying anything and justifying themself after the first days of confinement be like",
    "image" : "/img/eating_anything.gif",
    "date" : ISODate("2020-03-31T15:39:23.052Z"),
    "__v" : 0
})

db.items.insert({
    "_id" : ObjectId("5e8ee635d7b1600013655bbd"),
    "is_active" : true,
    "category" : "containment",
    "pseudo" : "EverySingleOne",
    "name" : "Toilet Paper Fever",
    "details" : "Wondering where all the toilet paper gone?",
    "image" : "/img/toilet_paper.gif",
    "date" : ISODate("2020-04-09T09:09:09.998Z"),
    "__v" : 0
})

db.items.insert({
    "_id" : ObjectId("5e948a3ad7b1600013655bbf"),
    "is_active" : false,
    "category" : "flag",
    "pseudo" : "W3LLD0NE",
    "name" : "Flag_second_part",
    "details" : "P0SSIBLE_W1TH_CUST0M_ID!_f878b1c38e20617a8fbd20d97524a515}",
    "image" : "/img/flag_second_part.gif",
    "date" : ISODate("2020-04-13T15:50:18.212Z"),
    "__v" : 0
})
