# -*- coding:utf-8 -*-

from flask import Flask, request, render_template, Response
from urllib.parse import urlsplit
import urllib.parse
from flask_httpauth import HTTPBasicAuth
from werkzeug.security import generate_password_hash, check_password_hash
from flask import make_response
import unicodedata

app = Flask(__name__)
auth = HTTPBasicAuth()

users = {
    "anonymous": generate_password_hash("anonymous"),
    "admin": generate_password_hash("Ba2aY2Zg8HRbsvbQEf8jWBAW7EykTLbdQtG")
}

@auth.verify_password
def verify_password(username, password):
    if username in users:
        return check_password_hash(users.get(username), password)
    return False

@app.before_request
def before_request_func():
    request.path = request.path.encode("idna").decode("ascii")


@app.route('/')
def main():
    if auth:
        username = auth.username()
    r = make_response(render_template('main.html', user=username))
    r.headers.add('Server_info', "Werkzeug/1.0.1 Python/3.7.2")
    return r


@app.route('/news')
@auth.login_required
def news():
    if auth:
        username = auth.username()

    return render_template('news.html', user=username)

@app.route('/admin-query')
@auth.login_required
def admin_empty_query():
    r = make_response(render_template('informations.html', user=auth.username(), location="http://aquaworld.sharkyctf.xyz"))
    r.headers.add('Server_info', "Werkzeug/1.0.1 Python/3.7.2")
    return r

@app.route('/admin-query<whatever>')
@auth.login_required
def admin(whatever):

    sharky = "http://aquaworld.sharkyctf.xyz"
    first_character = unicodedata.normalize("NFKC", whatever.encode("utf-8").decode('unicode-escape'))
    if first_character[0] != '?' and first_character[0] != '#':
        return Response("The first character has to announce a query or a fragment")

    path = sharky + whatever
    app.logger.info(path)
    url = urlsplit(path).netloc.rpartition("@")[2]
    app.logger.info(url)
    if url == "127.0.0.1" or auth.username() == "admin":
        if request.args.get('flag'):
            flag = "shkCTF{NFKC_normalization_can_be_dangerous!_8471b9b2da83011a07efc2899819da65}"
        else:
            flag = "Well done! Now send a request with a GET parameter flag to get your flag :D"
        r = make_response(render_template('admin.html', user=auth.username(), location=url, flag=flag))
        r.headers.add('Server_info', "Werkzeug/1.0.1 Python/3.7.2")
        return r
    r = make_response(render_template('informations.html', user=auth.username(), location=url))
    r.headers.add('Server_info', "Werkzeug/1.0.1 Python/3.7.2")
    return r


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5002)

