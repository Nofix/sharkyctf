https://bugs.python.org/issue36216

add this to your path on burp

aquaworld.sharkyctf.xyz/admin\uFF03@127.0.0.1

the \uFF03 is equivalent to # after an NFKC normalization, it is considered as a part of the url by the navigator, but as a correct url by the website the @ is interpreted as a part of the url, so what is after will be considered as the current netlocation.

et on accède à la page admin : `shkCTF{NFKC_normalization_can_be_dangerous!_8471b9b2da83011a07efc2899819da65}`

More infos on : https://url.spec.whatwg.org/#parsers
https://fr.wikipedia.org/wiki/Normalisation_Unicode
