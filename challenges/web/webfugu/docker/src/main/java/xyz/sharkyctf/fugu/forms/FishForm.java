package xyz.sharkyctf.fugu.forms;

public class FishForm {

	private String name;
	private int discoveryYear;
	private String discovererName;
	
	public FishForm() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDiscoveryYear() {
		return discoveryYear;
	}

	public void setDiscoveryYear(int discoveryYear) {
		this.discoveryYear = discoveryYear;
	}

	public String getDiscovererName() {
		return discovererName;
	}

	public void setDiscovererName(String discovererName) {
		this.discovererName = discovererName;
	}


	
}
