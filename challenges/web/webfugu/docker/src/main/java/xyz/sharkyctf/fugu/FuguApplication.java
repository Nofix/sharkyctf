package xyz.sharkyctf.fugu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuguApplication {

	public static void main(String[] args) {
		SpringApplication.run(FuguApplication.class, args);
	}

}
