import random
import string

with open("usernames.txt") as file_in:
    lines = []
    for line in file_in:
        print("db.users.insert({")
        print("\"moneyspent\" : " + str(random.randint(1, 100) * 1000) + ",")
        print("\"username\" : \"" + line.rstrip("\n") + "\",")
        print("\"roles\"     : [],")
        print("\"password\" : \"" + ''.join(random.choices(string.ascii_uppercase + string.ascii_lowercase + string.digits, k=15)) + "\"")
        print("})")
