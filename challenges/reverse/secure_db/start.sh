#!/bin/bash

cd ./sources/client/ && pwd && gcc ./sources/client.c ./sources/aes.h ./sources/aes.c -o ../../challenge/secure_db -no-pie  -masm=intel -O4 -falign-labels=9 -m32 -DXORED_WITH_BP -DHOSTNAME=\"149.202.221.103\" -DANTI_FUNCTION_ENTRY=TRUE && printf "\x00" | dd of=../../challenge/secure_db bs=1 seek=32 count=1 conv=notrunc && cd -

docker-compose build && docker-compose up -d
