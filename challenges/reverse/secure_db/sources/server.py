#!/usr/bin/env python3

import socket
import _thread
# import numpy
import struct
import binascii
from Crypto.Cipher import AES
HOST = '0.0.0.0'
PORT = 7001
fileToSend = '/database/db.enc'


def cypherBlock(chunk):
    iv = "\x71\x01\x02\x03\x04\x48\x06\x07\x88\x09\x0a\xfe\x0c\x4d\x0e\x0f"
    key = b'T4h7s_4ll_F0lks\x00'
    encryptor = AES.new(key, AES.MODE_CBC, iv)
    padding = 16 - len(chunk)
    return encryptor.encrypt(chunk + (chr(0 + 1* padding) * padding))


def chunks(l, n):
    for i in range(0, len(l), n):
        yield l[i:i+n]

def new_client(clientsocket, addr):
    print('Connected from ', addr)
    with open(fileToSend, "rb") as file:
        fileContent = file.read()
    fileLen = len(fileContent)
    chunksToSend = list(chunks(fileContent, 16))
    for chunk in chunksToSend:
        clientsocket.send(struct.pack('<i', len(chunk)))
        clientsocket.send((chunk))
        # print("sending a chunk")
    print('File sent.')
    clientsocket.close()



print("Starting Server")
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((HOST, PORT))
sock.listen(10000)
print("Server is listening for connections...")
while True:
    conn, addr = sock.accept()
    _thread.start_new_thread(new_client,(conn,addr))
sock.shutdown()
sock.close()
