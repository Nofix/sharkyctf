EzDump - Build Me
=================

Il "suffit" de build le profil du dump.
Ensuite un simple linux_bash permet de donner le premier flag.

shkCTF{l3ts_st4rt_th3_1nv3st_75cc55476f3dfe1629ac60}

EzDump - Starting Block 
=======================

Ensuite il faut trouver le process à l'origine de l'attaque:
linux_pstree et linux_pslist donnent les solutions.

shkCTF{2854:ncat:2020-05-07 14:56:54}

EzDump - Entry Point
====================

Il faut trouver comment le processus netcat à été lancé, trouver la backdoor donne :

shkCTF{th4t_w4s_4_dumb_b4ckd00r_86033c19e3f39315c00dca}

Dans le bash_history on voit que le mec clone un git.
La solution est de clone en local le git de PythonBackup déposé sur mon repo.
Il faut chercher, la backdoor est cachée comme un tard dans snapshot.py sur la fonction generate_snapshot.py

EzDump - Attacker
=================

linux_netstat | grep ESTABLISHED donne la soluton pour les ip
pour la commande python qu'il a lancé :
on cherche le pid du processus bash ayant lancé le python via linux_pstree : soit “2876”
ensuite : linux_yarascan -Y “python” -p 2876

on retrouve alors plusieurs apparitions de la célèbre commande : python -c 'import pty; pty.spawn("/bin/bash")'

shkCTF{192.168.49.1:12345:python -c 'import pty; pty.spawn("/bin/bash")'}

EzDump - Compromised
====================

Maintenant que la backdoor a été identifiée, on l'a supprimé mais on a peur que l'utilisateur ait trouvé un moyen de s'identifier sur l'un de nos users. Retrouvez la backdoor lui permettant cela :

Pour le coup ya un hint dans le linux_bash, la dernière ligne est un vim /etc/rc.local :
si tu strings sur le dump comme un crevard à base de “strings | grep -A 20 -B 20” partir de cet indice tu peux vite trouver un indice avec le début du script bash + un commentaire qui commence par “Well played”
donc si tu restrings comme un crevard sur “Well played” le flag tombe malheureusement ><

La méthode plus legit c'est de linux_yarascan -Y “#!” -p 3196

shkCTF{rc.l0c4l_1s_funny_be2472cfaeed467ec9cab5b5a38e5fa0}

EzDump - Backdoor
=================

Pour le coup perso j'ai grep comme un port à partir de la commande vim /etc/rc.local pour retrouver l'ensemble des commandes tappée par le mec, dedans on trouve :  

insmod sysemptyrect.ko crc65_key="1337tibbartibbar"

Pour retrouver la base adresse de la backdoor en mémoire, il faut : linux_lsmod
Ensuite tu retrouves la base address à côté du nom du module :  ffffffffc0a14020

shkCTF{sysemptyrect.ko:1337tibbartibbar:0xffffffffc0a14020}

