#!/bin/sh
# This is part of docker entry point, not the challenge. Do not delete this file.

set -e
sleep 2
# Apache gets grumpy about PID files pre-existing
rm -f /usr/local/apache2/logs/httpd.pid

#find /var/lib/mysql/mysql -exec touch -c -a {} + && service mysql stop && mysqld_safe --skip-grant-tables &
#sleep 1

#/etc/init.d/mysql start
#rm -rf /var/lib/mysql/data/
#mysql -uroot -h localhost -P 3306 < /root/setup_db.sql
#mysql --host=localhost --user=gitea --password=B33r_Bamboo_Michael --database=gitea < /root/gitea.sql

# Setup a cron schedule
echo "SHELL=/bin/bash
* * * * * /usr/bin/python -c \"import backup;backup.backup('/var/www/html/blog', '/var/www/html/52e8b95db9d298bd03741e99abe57c8c1ff1fbd80bd94c366a7574baac7b1180/backup.zip').run()\" 2>&1
# " > /etc/cron.d/backup
crontab /etc/cron.d/backup
cron -f &

su - git -c "/usr/local/bin/gitea web -c /etc/gitea/app.ini &"


/usr/sbin/apache2ctl start
/usr/sbin/sshd -D
