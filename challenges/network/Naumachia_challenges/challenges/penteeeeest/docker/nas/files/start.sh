#!/bin/sh
set -e

# Setup a cron schedule

crontab /etc/cron.d/backup_website
cron -f &

/usr/sbin/sshd -D
