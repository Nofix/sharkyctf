#!/usr/bin/env python3

# Before : export WEB3_INFURA_API_KEY=6e8cc4f13d12484c8b1e4cda0d2baae3

from web3.auto.infura.ropsten import w3
from eth_utils import remove_0x_prefix, to_int, to_checksum_address, to_hex
import sys

if len(sys.argv) != 4:
	print("[+] Usage : python3 retrieveSlot.py <KEY_VALUE> <SLOT_MAPPING> <SLOT_ARRAY>")
	exit()

HOLDER_ADDRESS = sys.argv[1]
SLOT_MAPPING = sys.argv[2]
SLOT_INDEX = sys.argv[3]

pos = SLOT_MAPPING.rjust(64, '0')
key = remove_0x_prefix(HOLDER_ADDRESS).rjust(64, '0').lower()
print("[+] Key : " + key)
print("[+] Pos : " + pos)
storage_key = to_hex(w3.sha3(hexstr=key + pos))
index = to_hex(w3.sha3(hexstr="0x" + SLOT_INDEX.rjust(64, '0')))

print("Sha3 index array : " + index)
print("Sha3 index key : " + storage_key)
print("----> " + to_hex(2**256 - int(index, 16) + int(storage_key, 16)))
