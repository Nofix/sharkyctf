<?php
require 'configuration.php';

// visible
$algo="AES-128-CBC";
$iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($algo));


function encrypt($plaintext, $iv, $key, $cipher)
/**
 * Encrypt a plaintext
 *
 * Return ciphertext in base64.
 */
{
    $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    $ciphertext = base64_encode($iv.$ciphertext_raw);
    $ciphertext = trim(preg_replace('/\s+/', ' ', $ciphertext));
    return $ciphertext;
}


// Try and connect using the info above.
$con = mysqli_connect($DATABASE_HOST, $DATABASE_USER, $DATABASE_PASS, $DATABASE_NAME);
if ( mysqli_connect_errno() ) {
	// If there is an error with the connection, stop the script and display the error.
	die ('Failed to connect to MySQL: ' . mysqli_connect_error());
}

// Now we check if the data from the login form was submitted, isset() will check if the data exists.
if ( !isset($_POST['username'], $_POST['password']) ) {
	// Could not get the data that should have been sent.
	die ('Please fill both the username and password field!');
}

// Prepare our SQL, preparing the SQL statement will prevent SQL injection.
if ($stmt = $con->prepare('SELECT id, password, is_an_administrator FROM users WHERE username = ?')) {
	// Bind parameters (s = string, i = int, b = blob, etc), in our case the username is a string so we use "s"
	$stmt->bind_param('s', $_POST['username']);
	$stmt->execute();
	// Store the result so we can check if the account exists in the database.
	$stmt->store_result();

	if ($stmt->num_rows > 0) {
        $stmt->bind_result($id, $password, $is_admin);
        $stmt->fetch();
        // Account exists, now we verify the password.
        // Note: remember to use password_hash in your registration file to store the hashed passwords.
        if (password_verify($_POST['password'], $password)) {
            $name = htmlentities($_POST['username'], ENT_QUOTES, 'UTF-8');
            echo 'Welcome ' . $name . '!';
            //$authentication_token = json_encode('{"id": "' . $id . '", "is_admin": ' . $is_admin . ', "username": "' . $name . '"');
            $authentication_token = json_encode(
                array(
                    "id" => $id,
                    "is_admin" => $is_admin,
                    "username" => $name
                )
            );
            $authentication_token = encrypt($authentication_token, $iv, $SECRET_KEY, $algo);
            setcookie('authentication_token', $authentication_token);
            setcookie('debug', "false");
            header('Location: /profile.php');
            exit();
        } else {
            echo '<p>Incorrect password!</p>';
            echo "<p><a href='/index.html'>return to login page</a></p>";
        }
    } else {
        echo 'Incorrect username!';
        echo "<p><a href='/index.html'>return to login page</a> or <a href='/register.html'>create an account</a></p>";
    }
	$stmt->close();
}
