#!/bin/bash

cd docker
docker build . -t give_away_0
docker run -d --name give_away_0 -p 20333:20333 -m 32m --memory-swap 32m --read-only --restart unless-stopped --cpus=".1" give_away_0

