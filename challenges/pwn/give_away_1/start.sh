#!/bin/bash

cd docker
docker build . -t give_away_1
docker run -d --name give_away_1 -p 20334:20334 -m 32m --memory-swap 32m --read-only --restart unless-stopped --cpus=".1" give_away_1

