#!/bin/bash

cd docker
docker build . -t kikoo_4_ever
docker run -d --name kikoo_4_ever -p 20337:20337 -m 32m --memory-swap 32m --read-only --restart unless-stopped --cpus=".1" kikoo_4_ever

